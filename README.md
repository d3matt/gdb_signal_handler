This is an example of calling gdb from a signal handler using signal safe methods

Example output:

    [ sto62746@matt-vm ~/work/signal  :) ] $ make
    gcc -lpthread -Wall -g -o thread_segv thread_segv.c
    [ sto62746@matt-vm ~/work/signal  :) ] $ ./thread_segv 
    in thread
    SEGV!!!!
    + echo 'parent is 15084'
    parent is 15084
    + gdb -p 15084 -batch -ex 'thread apply all bt full'
    [New LWP 15085]
    [Thread debugging using libthread_db enabled]
    Using host libthread_db library "/lib64/libthread_db.so.1".
    0x00007f67a451b76d in nanosleep () from /lib64/libc.so.6

    Thread 2 (Thread 0x7f67a4452700 (LWP 15085)):
    #0  0x00007f67a451b76d in nanosleep () from /lib64/libc.so.6
    No symbol table info available.
    #1  0x00007f67a451b604 in sleep () from /lib64/libc.so.6
    No symbol table info available.
    #2  0x00000000004008c2 in sig_handler (signo=11) at thread_segv.c:36
            child = 15086
            parent = 15084
            status = 0
    #3  <signal handler called>
    No symbol table info available.
    #4  0x000000000040090b in bar () at thread_segv.c:52
            x = 0x0
    #5  0x000000000040092b in foo () at thread_segv.c:58
    No locals.
    #6  0x0000000000400958 in segv_thread (arg=0x0) at thread_segv.c:65
    No locals.
    #7  0x00007f67a481b60a in start_thread () from /lib64/libpthread.so.0
    No symbol table info available.
    #8  0x00007f67a4555a9d in clone () from /lib64/libc.so.6
    No symbol table info available.

    Thread 1 (Thread 0x7f67a4c33700 (LWP 15084)):
    #0  0x00007f67a451b76d in nanosleep () from /lib64/libc.so.6
    No symbol table info available.
    #1  0x00007f67a451b604 in sleep () from /lib64/libc.so.6
    No symbol table info available.
    #2  0x0000000000400991 in main () at thread_segv.c:76
            _thread = 140083114354432
    + kill -9 15084
    Killed
