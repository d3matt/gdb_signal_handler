#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

char * new_argv[2] = {
    "./gdb.sh",
    NULL /* set the rest of the elements to NULL */
};
volatile int FLAG = 0;

void sig_handler(int signo)
{
    pid_t child;
    pid_t parent = getpid();
    int status;

    if (signo == SIGSEGV) {
        //note: we should increment an atomic number here and only cotinue if we were the first...
        status = __sync_fetch_and_add(&FLAG, 1);
        if(status == 0)
        {
            child = fork();
            if(child == 0)
            {
                execv(new_argv[0], new_argv);
                //if we get here, just kill the parent...  we're doomed
                kill(parent, 9);
            }
            else if (child == -1)   //fork can fail...
            {
                _exit(-1);
            }
        }
        while(1) sleep(1);
    }
}


void init_handler(void)
{
    if (signal(SIGSEGV, sig_handler) == SIG_ERR) printf("\ncan't catch SIGSEGV\n");
}

void bar()
{
    int *x;
    x=0;
    printf("SEGV!!!!\n");
    while(1) {
        (*x)++;
        x++;
    }
}
void foo()
{
    bar();
}

void * segv_thread(void * arg)
{
    printf("in thread\n");
    sleep(1);
    foo();
    return NULL;
}

int main(void)
{
    // A long long wait so that we can easily issue a signal to this process
    pthread_t _thread;

    init_handler();
    pthread_create(&_thread, NULL, &segv_thread, NULL);
    while(1) sleep(1);
    return 0;
}
