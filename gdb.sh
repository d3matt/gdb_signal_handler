#!/bin/bash -x

echo "parent is $PPID"

gdb -p $PPID -batch -ex 'thread apply all bt full'

kill -9 $PPID
